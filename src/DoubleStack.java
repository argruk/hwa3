import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.*;
public class DoubleStack implements Cloneable{

   private LinkedList<Double> list;

   public static void main (String[] argum) {
      
      DoubleStack stack1 = new DoubleStack();
      System.out.println(stack1.interpret("2 5 9 ROT - +"));
      System.out.println(stack1.interpret("2 5 9 ROT + SWAP -"));
   }

   DoubleStack(){
      list = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {

      DoubleStack returnElement = new DoubleStack();

      int i = 0;

      while (i<list.size()){
         returnElement.list.add(list.get(i));
         i++;
      }

      return returnElement;
   }

   public boolean stEmpty() {

      return list.size() == 0;
   }

   public void push (double a) {
      list.push(a);
   }

   public double pop() {
      if (this.stEmpty()) throw new RuntimeException("Stack underflow!");
      return list.pop();
   }

   public void op (String s) {
      if (list.size() <= 1){
         throw new RuntimeException("Insufficient number of elements in stack! Current stack is: <"+toString()+">");
      }
      double topValue = list.pop();
      double nextValue = list.peek();

      switch (s){
         case "*":
            list.set(0,topValue*nextValue);
            break;
         case "/":
            list.set(0,nextValue/topValue);
            break;
         case "+":
            list.set(0,topValue+nextValue);
            break;
         case "-":
            list.set(0,nextValue-topValue);
            break;
         default:
            throw new RuntimeException("Illegal symbol: " + s+".");
      }
   }
  
   public double tos() {
      if (this.stEmpty()){
         throw new RuntimeException("No elements in the stack!");
      }
      return list.peek();
   }

   @Override
   public boolean equals (Object o) {
      if (o==this) return true;
      if (!(o instanceof DoubleStack)) return false;


      DoubleStack receivedObject = (DoubleStack)o;

      if (receivedObject.list.size() != list.size()) return false;

      for (int i = 0; i < list.size(); i++) {
         if (!list.get(i).equals(receivedObject.list.get(i))){
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuffer buildingString = new StringBuffer();
      int i = list.size();
      while (i > 0){
         buildingString.append(list.get(i-1));
         buildingString.append(" ");
         i--;
      }

      return buildingString.toString();
   }

   public static double interpret (String pol) throws RuntimeException {

      StringTokenizer tokenizered= new StringTokenizer(pol);
      if (tokenizered.countTokens() == 0){
         throw new RuntimeException("Empty string!");
      }
      List<String> tokens = new ArrayList<String>();
      while (tokenizered.hasMoreTokens()){
         tokens.add(tokenizered.nextToken());
      }

      DoubleStack stack = new DoubleStack();
      double topValue;
      double nextValue ;
      for (int i = 0; i < tokens.size(); i++) {
         switch (tokens.get(i)){
            case "*":
               if (stack.list.size() <= 1){
                  throw new RuntimeException("Insufficient number of elements in stack from expression: "+pol+".");
               }
               try{
                  topValue = stack.list.pop();
                  nextValue  = stack.list.peek();
                  stack.list.set(0,nextValue*topValue);
               }  catch (NoSuchElementException e){
                  throw new RuntimeException("Cannot perform multiplication" + " in expresion " + pol);
               }
               break;
            case "/":
               if (stack.list.size() <= 1){
                  throw new RuntimeException("Insufficient number of elements in stack from expression: "+pol+".");
               }
               try{
                  topValue = stack.list.pop();
                  nextValue  = stack.list.peek();
                  stack.list.set(0,nextValue/topValue);
               }  catch (NoSuchElementException e){
                  throw new RuntimeException("Cannot perform division" + " in expresion " + pol);
               }
               break;
            case "+":
               if (stack.list.size() <= 1){
                  throw new RuntimeException("Insufficient number of elements in stack from expression: "+pol+".");
               }
               try{
                  topValue = stack.list.pop();
                  nextValue  = stack.list.peek();
                  stack.list.set(0,nextValue+topValue);
               }  catch (NoSuchElementException e){
                  throw new RuntimeException("Cannot perform addition" + " in expresion " + pol);
               }

               break;
            case "-":
               if (stack.list.size() <= 1){
                  throw new RuntimeException("Insufficient number of elements in stack from expression: "+pol+".");
               }
               try{
                  topValue = stack.list.pop();
                  nextValue  = stack.list.peek();
                  stack.list.set(0,nextValue-topValue);
               }  catch (NoSuchElementException e){
                  throw new RuntimeException("Cannot perform subtraction" + " in expresion " + pol);
               }
               break;
            case "SWAP":
               if (stack.list.size() <= 1){
                  throw new RuntimeException("Insufficient number of elements in stack: " + pol+ "!");
               }
               topValue = stack.list.pop();
               nextValue = stack.list.peek();
               stack.list.set(0,topValue);
               stack.push(nextValue);
               break;
            case "ROT":
               if (stack.list.size() <= 2){
                  throw new RuntimeException("Insufficient number of elements in stack: " + pol+ "!");
               }
               double firstElement = stack.list.pop();
               double secondElement = stack.list.pop();
               double thirdElement = stack.list.pop();
               double temp = firstElement;
               firstElement = thirdElement;
               thirdElement = secondElement;
               secondElement = temp;
               stack.list.push(thirdElement);
               stack.list.push(secondElement);
               stack.list.push(firstElement);
               break;
            default:
               try {
                  stack.push(Double.parseDouble(tokens.get(i)));
               }catch (NumberFormatException e){
                  throw new RuntimeException("Cannot convert illegal symbol "+tokens.get(i)+"from expression: "+pol+".");
               }
         }
      }

      if (stack.list.size() > 1){
         throw new RuntimeException("Redundant elements on top of stack in expression " + pol + ". Operation is illegal");
      }
      return stack.list.peek();
   }

}

